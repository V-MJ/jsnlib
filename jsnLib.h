#ifndef JSNLIB_H
#define JSNLIB_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#define jsnChunkLen 16

//8bits
typedef uint8_t u8;
typedef int8_t  i8;
typedef uint8_t byte;

//16bits
typedef uint16_t u16;
typedef int16_t  i16;

//32bits
typedef uint32_t u32;
typedef int32_t  i32;
typedef float    f32;

//64bits
typedef uint64_t u64;
typedef int64_t  i64;
typedef double   f64;



typedef enum {
	jsnNull		= 1,	//value is but is nul
	jsnNumber	= 2,	//a number value
	jsnString	= 3,	//string of text
	jsnArray	= 4,	//use direct index access
	jsnObject	= 5,	//use iterative hash comparission
	jsnTrue		= 6,
	jsnFalse	= 7
} jsnType;
typedef enum {
	unsignedV	= 1,
	signedV		= 2,
	floatV		= 3,
} jsnNumType;
typedef struct{
	union{
		u64	uVal;
		i64	iVal;
		f64	fVal;
	};
	jsnNumType type;
} jsnNumber_t;

typedef struct jsnChunk jsn_t;
typedef struct{
	union{
		jsnNumber_t	number;
		char*		string;
		jsn_t*		data;
	};
	jsnType valueType;
} jsnValue_t;
struct jsnChunk{
	struct jsnChunk* next;//incase we have more data that jsnChunk can handle
	u64		keys[jsnChunkLen];
	jsnValue_t	values[jsnChunkLen];
	u16		fill;
};

typedef enum {
	jsnNoError = 0,
	jsnNoFile = -1,
	jsnParseError = -2,
	jsnNoMemory = -3,
	jsnUnexpectedChar = -4,
	jsnValueIsNotArrayOrObject = -5,
	jsnValueIsNotObject = -6,
	jsnIndexOutOffBounds = -7,
	jsnNoValueForKey = -8,
	jsnValueIsNotNumber = -9,
	jsnNoSuchNumberType = -10,
	jsnUnableToConvert = -11,
	jsnValueIsNotString = -12,
	jsnStringPointsToNull = -13,
	jsnValueInNull = -14,
	jsnInvalidType = -15
} jsnErr_t;


char* loadFile(char* filePath);
int loadFileString(char* filePath, char** str);//{//}, int* len){
u64 shash(char* url);

//init and destruct funcions
jsnErr_t parseJsn	(jsnValue_t** write,	char* path);
void freeJsn		(jsnValue_t* data);
//functions to access data
jsnErr_t accessIndex	(jsnValue_t* val, u64 index, jsnValue_t** rVal);//access array/object
jsnErr_t accessKey	(jsnValue_t* val, char* key, jsnValue_t** rVal);//access object
jsnErr_t contentLen	(jsnValue_t* val, u64* len);//how many values are stored in the array/object
jsnErr_t getNumber	(jsnValue_t* val, jsnNumType type, void* num);//get a number of wanted type	<- booleans are turned to numbers -1 and 0 for true and false respectively
jsnErr_t getString	(jsnValue_t* val, char** str);//get ponter to the string
jsnErr_t getType	(jsnValue_t* val, jsnType* type);//get the type of the data
char* getErrorString	(jsnErr_t err);

//internal parse fuctions
//jsnErr_t parseJsn	(jsn_t** write,		char* path);
jsnErr_t parseValue	(jsnValue_t* target,	char* json, u64* jsonIndex);//number, string, bools OK
jsnErr_t parseObject	(jsn_t** write,		char* json, u64* jsonIndex);//seems to function when there are less than jsnChunkLen of elements
jsnErr_t parseArray	(jsn_t** write,		char* json, u64* jsonIndex);
jsnErr_t parseString	(char** rv,		char* json, u64* jsonIndex);//mostly works <- some how \" escape doesn't work properly
jsnErr_t parseNumber	(jsnNumber_t* write,	char* json, u64* jsonIndex);//should work


#endif//JSNLIB_H