#include <math.h>
#include "jsnLib.h"


int isWhiteSpace(char c){
	if((c == ' ') || (c == '\n') || (c == '\r') || (c == '\t'))
		return 1;
	return 0;
}
int isExpectedChar(char c,  char* compA, int compL){
	for(int i = 0; i < compL; i++){
		if(c == compA[i])
			return 1;
	}
	return 0;
}

void skipWhiteSpace(char* json, u64* jsonIndex){
	while(isWhiteSpace(json[*jsonIndex])){
		*jsonIndex += 1;
	}
}



/*hashing library*/
u8 u8RL(u8 val, u8 rotate){
	rotate %= 8;
	return (u8)(val << rotate) | (u8)(val >> (8 - rotate)); //CHAR_BIT
}
u8 u8RR(u8 val, u8 rotate){
	rotate %= 8;
	return (u8)(val >> rotate) | (u8)(val << (8 - rotate));
}

u64 shash(char* url){	//should be replaced with something that provides better randoms hashes

/*	u64 rdata = 5381;
	u8 c = 0;
	while(*url != '\0'){
		c = *url;
		rdata = ((rdata << 5) + rdata) + c;
		url++;
	}*/

	u8 data[8] = {'P', '3', 'n', '1', '5', 'L', '0', 'l'};
	if(url != NULL){
		u64 indx = 0;
		while(url[indx] != '\0'){

			data[0] ^= (u8)url[indx] ^ data[7];
			data[1] += data[0];
			data[2] ^= u8RL(data[1], 5);
			data[3] += data[2] ^ data[6];
			data[4] ^= data[3] + data[0];
			data[5] += u8RR(data[4], 3);
			data[6] ^= data[5];
			data[7] += data[6];
			indx++;
		}
	}
	//combine bytes to a u64
	u64 rdata = 0;
	rdata = (rdata << 8) | data[6];
	rdata = (rdata << 8) | data[1];
	rdata = (rdata << 8) | data[7];
	rdata = (rdata << 8) | data[5];
	rdata = (rdata << 8) | data[4];
	rdata = (rdata << 8) | data[0];
	rdata = (rdata << 8) | data[2];
	rdata = (rdata << 8) | data[3];
	
	return rdata;
}

char* loadFile(char* filePath){

	//openfile
	FILE *f = fopen(filePath, "rb");

	fseek(f, 0, SEEK_END);
	uint64_t fsize = (uint64_t)ftell(f);

	fseek(f, 0, SEEK_SET);
	//copy to mem
	char *buffer = malloc(sizeof(char)*fsize+1);
	printf("%ld", fread(buffer, fsize, 1, f));
	

	buffer[fsize] = '\0';
	fclose(f);

	//return pointer
	return buffer;
}

int loadFileString(char* filePath, char** str){//}, int* len){
	//openfile
	FILE *f = fopen(filePath, "rb");
	if(!f)
		return -1;

	fseek(f, 0, SEEK_END);
	uint64_t fsize = (uint64_t)ftell(f);

	fseek(f, 0, SEEK_SET);
	//copy to mem
	char *buffer = malloc(sizeof(char)*fsize+1);
	if(!buffer)
		return -2;
	
	fread(buffer, fsize, 1, f);


	buffer[fsize] = '\0';
	fclose(f);

	*str = buffer;
	return 0;
}



jsnErr_t parseNumber(jsnNumber_t* write, char* json, u64* jsonIndex){
	char sign		= 1;
	uint64_t intager	= 0;
	uint64_t fraction	= 0;
	uint64_t fractionLen	= 0;
	char digts[] = {'1','2','3','4','5','6','7','8','9','0'};

	//check sign
	if(json[*jsonIndex] == '-'){
		sign = -1;
		*jsonIndex +=1;
	}
	
	
	//get intager
	if(!isExpectedChar(json[*jsonIndex], digts, 10)){

		return jsnUnexpectedChar;
	}
	intager = json[*jsonIndex]-'0';
	*jsonIndex+=1;
	while(isExpectedChar(json[*jsonIndex], digts, 10)){

		intager = intager*10+(json[*jsonIndex]-'0');
		*jsonIndex+=1;
	}

	//get fraction
	if(json[*jsonIndex] == '.'){
		*jsonIndex+=1;
		while(isExpectedChar(json[*jsonIndex], digts, 10)){
			fraction = fraction*10+(json[*jsonIndex]-'0');
			fractionLen++;
			*jsonIndex+=1;
		}
	}

	//convert parsed data to usabel numbers
	if((sign < 1) | fraction){//number is signed
		if(fraction){//float
			write->fVal = (((double)intager) + (((double)(fraction))/(pow(10.0,(double)fractionLen))))*((double)sign);
			write->type = floatV;
		}else{//signed int
			write->iVal = intager*sign;
			write->type = signedV;
		}
	}else{//number is unsigned int
		write->uVal = intager;
		write->type = unsignedV;
	}

	return 0;
}
jsnErr_t parseString(char** rv, char* json, u64* jsonIndex){
	//find begining of string

	if(json[*jsonIndex] != '"'){
		return jsnUnexpectedChar;
	}
	*jsonIndex += 1;
	
	u64 partLen = *jsonIndex;
	//get string lenght
	while(json[partLen] != '"'){

		if(json[partLen] == '\\'){
			partLen++;
		}
		partLen++;
	}

	rv[0] = malloc(sizeof(char)*(partLen-(*jsonIndex)+1));
	if(*rv == NULL)
		return jsnNoMemory;


	//parse the text to data
	u64 wi = 0;
	int i = 0;
	while(json[*jsonIndex] != '"'){
		rv[0][i] = json[*jsonIndex];
		if(json[*jsonIndex] == '\\'){
			*jsonIndex+=1;
			i++;
			rv[0][i] = json[*jsonIndex];
		}

		*jsonIndex += 1;
		i++;
	}
	*jsonIndex += 1;
	rv[0][i] = '\0';

	return 0;
}
jsnErr_t parseArray(jsn_t** write, char* json, u64* jsonIndex){
	//check for proper begin

	if(json[*jsonIndex] != '[')
		return jsnUnexpectedChar;
	*jsonIndex += 1;
	
	//allocate some memory for array
	*write = malloc(sizeof(jsn_t));
	if(*write == NULL)
		return jsnNoMemory;
	memset(*write, 0, sizeof(jsn_t));

	//check for array of 0 lenght
	skipWhiteSpace(json, jsonIndex);
	if(json[*jsonIndex] == ']'){
		*jsonIndex += 1;
		return 0;
	}

	jsn_t* arr = *write;
	for(;;){

		skipWhiteSpace(json, jsonIndex);
		//read value
		int err = parseValue(&arr->values[arr->fill], json, jsonIndex);
		if(err)
			return err;
		arr->fill++;

		//allocate moredata if neccissary
		if(arr->fill == jsnChunkLen){
			arr->next = malloc(sizeof(jsn_t));
			if(arr->next == NULL)
				return jsnNoMemory;
			arr = arr->next;
			memset(arr, 0, sizeof(jsn_t));
		}

		if(json[*jsonIndex] == ','){
			*jsonIndex += 1;
		}else{
			skipWhiteSpace(json, jsonIndex);
			if(json[*jsonIndex] == ']'){
				*jsonIndex += 1;
				return 0;
			}else{

				return jsnUnexpectedChar;
			}
		}
	}
}
jsnErr_t parseObject(jsn_t** write, char* json, u64* jsonIndex){
	*write = malloc(sizeof(jsn_t));
	memset(*write, 0, sizeof(jsn_t));
	//fill until full

	if(json[*jsonIndex] != '{'){
		return jsnUnexpectedChar;
	}
	*jsonIndex += 1;//skip the beginig {
	//check for object of 0 lenght
	skipWhiteSpace(json, jsonIndex);
	if(json[*jsonIndex] == '}'){
		*jsonIndex += 1;
		return 0;
	}

	jsn_t* obj = *write;
	for(;;){
		//parse the key symbol
		char* str = NULL;

		skipWhiteSpace(json, jsonIndex);

		int err = parseString(&str, json, jsonIndex);
		if(err)
			return err;

		u64 strHash = shash(str);

		free(str);

		skipWhiteSpace(json, jsonIndex);//skip before :

		if(json[*jsonIndex] != ':')
			return jsnUnexpectedChar;
		*jsonIndex += 1;
		skipWhiteSpace(json, jsonIndex);

		//check if appendable is full
		if(obj->fill == jsnChunkLen){
			obj->next = malloc(sizeof(jsn_t));
			if(obj->next == NULL)
				return jsnNoMemory;
			obj = obj->next;
			memset(obj, 0, sizeof(jsn_t));
		}

		//append to
		obj->keys[obj->fill] = strHash;
		err = parseValue(&obj->values[obj->fill], json, jsonIndex);
		if(err)
			return err;

		obj->fill += 1;

		if(json[*jsonIndex] == ','){//object should have more values
			*jsonIndex += 1;
		}else if(json[*jsonIndex] == '}'){//end of object
			*jsonIndex += 1;
			return 0;
		}else{
			return jsnUnexpectedChar;
		}
	}
}

jsnErr_t parseValue(jsnValue_t* trget, char* json, u64* jsonIndex){
	jsnErr_t rv = 0;
	char fChar = json[*jsonIndex];
	char numLegal[] = {'0','1','2','3','4','5','6','7','8','9','-','.'};

	skipWhiteSpace(json, jsonIndex);

	if(fChar == '{'){//object
		trget->valueType = jsnObject;
		rv = parseObject(&trget->data, json, jsonIndex);
	}else if(fChar == '['){//array
		trget->valueType = jsnArray;
		rv = parseArray(&trget->data, json, jsonIndex);
	}else if(fChar == '"'){//string
		trget->valueType = jsnString;
		rv = parseString(&trget->string, json, jsonIndex);
	}else if(fChar == 't'){//true
		trget->valueType = jsnTrue;
		*jsonIndex += 4;
	}else if(fChar == 'f'){//false
		trget->valueType = jsnFalse;
		*jsonIndex += 5;
	}else if(isExpectedChar(fChar, numLegal, 12)){ 
		trget->valueType = jsnNumber;
		rv = parseNumber(&trget->number, json, jsonIndex);
	}else if(fChar == 'n'){//null
		trget->valueType = jsnNull;
		*jsonIndex += 4;
	}
	else{
		rv = jsnUnexpectedChar;
	}
	skipWhiteSpace(json, jsonIndex);
	return rv;
}

jsnErr_t parseJsn(jsnValue_t** write, char* path){
	//*write = NULL;
	char* fileString = loadFile(path);
	if(fileString == NULL)
		return jsnNoFile;
	u64 jsIndx = 0;

	*write = malloc(sizeof(jsn_t));
	if(parseValue(*write, fileString, &jsIndx))
		return jsnParseError;

	free(fileString);
	return 0;
}
void freeJsnValue(jsnValue_t* data){
	if(data->valueType == jsnString){
		free(data->string);
	}else if((data->valueType == jsnArray) | (data->valueType == jsnObject)){
		jsn_t* num = data->data;
		while(num != NULL){
			for(int i = 0; i < num->fill; i++){
				freeJsnValue(&num->values[i]);
			}
			jsn_t* on = num;
			num = num->next;
			free(on);//free the object chunk
		}
	}
}

void freeJsn(jsnValue_t* data){
	freeJsnValue(data);
	free(data);
}


//functions to access data
jsnErr_t accessIndex(jsnValue_t* val, u64 index, jsnValue_t** rVal){//access array/object

	if(!((val->valueType == jsnArray) | (val->valueType == jsnObject)))
		return jsnValueIsNotArrayOrObject;

	jsn_t* head = val->data;
	uint64_t depth = 0;
	while(head != NULL){
		depth += head->fill;
		if(index <= depth){
			if(index%jsnChunkLen < head->fill){

				*rVal = &head->values[index%jsnChunkLen];
				return 0;
			}else{

				return jsnIndexOutOffBounds;
			}
		}
		head = head->next;
	}
	return jsnIndexOutOffBounds;
}
jsnErr_t accessKey(jsnValue_t* val, char* key, jsnValue_t** rVal){//access object
	if(val->valueType != jsnObject)
		return jsnValueIsNotObject;

	uint64_t hash = shash(key);
	jsn_t* head = val->data;
	while(head != NULL){
		for(int i = 0; i < head->fill; i++){
			if(hash == head->keys[i]){
				*rVal = &head->values[i];
				return 0;
			}
		}
		head = head->next;
	}
	return jsnNoValueForKey;
}
jsnErr_t contentLen(jsnValue_t* val, u64* len){//how many values are stored in the array/object
	*len = 0;
	u64 lenc = 0;
//	if((val->valueType != jsnArray) | (val->valueType != jsnObject))
	if(!((val->valueType == jsnArray) | (val->valueType == jsnObject)))
		return jsnValueIsNotArrayOrObject;
	jsn_t* arr = val->data;
	while(arr != NULL){
		lenc += arr->fill;
		arr = arr->next;
	}
	*len = lenc;
	return 0;
}
jsnErr_t getNumber(jsnValue_t* val, jsnNumType type, void* num){//get a number of wanted type	<- booleans are turned to numbers -1 and 0 for true and false respectively
	if(val->valueType == jsnTrue){
		*(int64_t*)num = 0;
		return 0;
	}else if(val->valueType == jsnTrue){
		*(int64_t*)num = -1;
		return 0;
	}else if(val->valueType != jsnNumber){
		return jsnValueIsNotNumber;
	}

	//handle type conversion prehaps json should just parse the integer, fraction exponent and signes and this would just decode that into each respective number type
	if(type == unsignedV){
		if(val->valueType == unsignedV)
			*(uint64_t*)num = (uint64_t)val->number.uVal;
		if(val->valueType == signedV)
			*(uint64_t*)num = (uint64_t)val->number.iVal;
		if(val->valueType == floatV)
			*(uint64_t*)num = (uint64_t)val->number.fVal;
	}else if(type == signedV){
		if(val->valueType == unsignedV)
			*(int64_t*)num = (int64_t)val->number.uVal;
		if(val->valueType == signedV)
			*(int64_t*)num = (int64_t)val->number.iVal;
		if(val->valueType == floatV)
			*(int64_t*)num = (int64_t)val->number.fVal;
	}else if(type == floatV){
		if(val->valueType == unsignedV)
			*(double*)num = (double)val->number.uVal;
		if(val->valueType == signedV)
			*(double*)num = (double)val->number.iVal;
		if(val->valueType == floatV)
			*(double*)num = (double)val->number.fVal;
	}else{
		return jsnNoSuchNumberType;
	}

	return 0;
}
jsnErr_t getString(jsnValue_t* val, char** str){//get ponter to the string
	if(val->valueType != jsnString)
		return jsnValueIsNotString;
	if(val->string ==NULL)
		return jsnStringPointsToNull;
	*str = val->string;
	return 0;
}
jsnErr_t getType(jsnValue_t* val, jsnType* type){//get the type of the data
	if((val->valueType == 0) | (val->valueType > 7))
		return jsnInvalidType;
	*type = val->valueType;
	return 0;
}

char* errorNames[] = {
	"jsnNoError",
	"jsnNoFile",
	"jsnParseError",
	"jsnNoMemory",
	"jsnUnexpectedChar",
	"jsnValueIsNotArrayOrObject",
	"jsnValueIsNotObject",
	"jsnIndexOutOffBounds",
	"jsnNoValueForKey",
	"jsnValueIsNotNumber",
	"jsnNoSuchNumberType",
	"jsnUnableToConvert",
	"jsnValueIsNotString",
	"jsnStringPointsToNull",
	"jsnValueInNull",
	"jsnInvalidType"
};

char* getErrorString(jsnErr_t err){
	char* rs = NULL;
	if((err*-1 > 0) & (err*-1 < 16)){
		rs = errorNames[err*-1];
	}
	return rs;
}