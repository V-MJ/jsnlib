#include "../jsnLib.h"

int indent = 0;
void printValue(jsnValue_t* val){
	for(int i = 0; i < indent; i++)
		printf("\t");
	switch(val->valueType){
		case jsnNull:
			printf("null");
			break;
		case jsnNumber:
			for(int i = 0; i < indent; i++)
				printf("\t");
			if(val->number.type == unsignedV){
				printf("%u", val->number.uVal);
			}else if(val->number.type == signedV){
				printf("%i", val->number.iVal);
			}else if(val->number.type == floatV){
				printf("%f", val->number.fVal);
			}else{
				printf("invalid Number Type");
			}
			break;
		case jsnString:
			printf("\"%s\"", val->string);
			break;
		case jsnArray:
			printf("%d[ ", val->data->fill);
			jsn_t* arr = val->data;
			while(arr != NULL){
				for(int i = 0; i < arr->fill; i++){
					printValue(&arr->values[i]);
					printf(", ");
				}
				arr = arr->next;
			}
			printf("]\n");
			break;
		case jsnObject:
			printf("{\n");
			indent++;
			jsn_t* obj = val->data;
			while(obj != NULL){
				for(int i = 0; i < obj->fill; i++){
					for(int i = 0; i < indent; i++)
						printf("\t");
					printf("%u : ", obj->keys[i]);
					printValue(&obj->values[i]);
					printf(",\n");
				}
				obj = obj->next;
			}
			indent--;
			printf("}");
			break;
		case jsnTrue:
			printf("true");
			break;
		case jsnFalse:
			printf("false");
			break;
		default:
			printf("???");
	}
}

int main(void){
	jsnValue_t* write = NULL;
	jsnErr_t err = parseJsn(&write, "test.json");
	if(err){
		printf("\n\terror: %s\n", getErrorString(err));
	}

	//print the internal representation of json
	printValue(write);

	freeJsn(write);
	return 0;
}

